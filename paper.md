---
title: Executing workflows in the cloud with WESkit  

tags:

- Global Alliance for Genomics and Health (GA4GH)
- Workflow Execution Service (WES)
- Tool Repository Service (TRS)

authors:

- name: Sven Twardziok  
  orcid: 0000-0002-0326-5704  
  affiliation: 1  
- name: Alexander Kanitz  
  orcid: 0000-0002-3468-0652  
  affiliation: 2,3  
- name: Philip Reiner Kensche  
  orcid: 0000-0003-1299-9600  
  affiliation: 1 
- name: Pietro Mandreoli  
  orcid: ???  
  affiliation: ???  
- name: Dan Plischke  
  orcid: ???  
  affiliation: ???  
- name: Igor Topolsky  
  orcid: 3???
  affiliation: ???  
- name: Kim Philipp Jablonski  
  orcid: 0000-0002-4166-4343
  affiliation: 3,4

affiliations:
 - name: German Cancer Research Center (DKFZ), Foundation under Public Law, Im Neuenheimer Feld 280, Heidelberg, Germany  
   index: 1
 - name: Biozentrum, University of Basel, Spitalstrasse 41, 4056 Basel, Switzerland  
   index: 2
 - name: SIB - Swiss Institute of Bioinformatics, Quartier Sorge - Batiment Amphipole, 1015 Lausanne, Switzerland  
   index: 3
 - name: Department of Biosystems Science and Engineering, ETH Zurich, Basel, 4058, Switzerland
   index: 4

date: DD Month YYYY ???
bibliography: paper.bib
authors_short: Twardziok et al. (2021) WESkit
group: BioHackrXiv
event: BioHackathon Europe 2021
---

# Tables & Figures

![WESkit architecture](./architecture.svg)  
Figure 1. WESkit architecture.
TRS-based workflow and MinIO-based data access were implemented during the
hackathon.

# Introduction

With the promise of the _"bringing computation to the data"_ principle, FAIR
[@FAIR_2016] cloud computing infrastructure is a key to managing the ongoing
explosion of data in the healthcare sector and life sciences. The Global
Alliance for Genomics and Health (GA4GH) [@GA4GH_2021], an international
policy-framing organization that aims at developing and establishing global
standards in personalized healthcare, defines a number of high-level technical
HTTP API standards for accessing and processing data in the cloud. These
specifications abstract over vendor-specific solutions with respect to workflow
languages and compute infrastructures, and they provide uniform mechanisms for
accessing bioinformatics tools, scientific analysis workflows and data.
Due to their modular design inspired by the "microservice" design pattern, they
are easy to adopt and enable service providers in academia and industry to
develop federated, FAIR cloud environments.


Here, we present the integration of support for fetching scientific data
analysis workflows via one of these APIs, the GA4GH [Tool Repository Service
(TRS)](https://github.com/ga4gh/tool-registry-service-schemas) API, into
[WESkit](https://gitlab.com/one-touch-pipeline/weskit), a service for triggering 
runs of such workflows in the cloud that is based on the GA4GH 
[Workflow Execution Service (WES)](https://github.com/ga4gh/workflow-execution-service-schemas) API.


The WES standard provides a uniform interface for the execution of scientific workflows such as
Nextflow [@Nextflow_2017] or Snakemake [@Snakemake_2021]. Both, the TRS and WES
APIs, thus help improve interoperability and reproducibility of (big) data
analysis in the life sciences.

## WESkit

[WESkit](https://gitlab.com/one-touch-pipeline/weskit/api) is a web service
that wraps Nextflow and Snakemake workflow engines in a WES API and
triggers their execution in response to incoming WES workflow run requests.
Support for additional workflow engines is planned and is facilitated by its
workflow engine-agnostic, modular design from the ground up. WESkit aims to
provide a scalable, robust user experience with support for multiple workload
managers (e.g., [Slurm](https://slurm.schedmd.com/) or
[LSF](https://www.ibm.com/docs/en/slsfh)).


WESkit's systems architecture is displayed in Figure 1. The WESkit REST server
receives the clients' requests via its REST API (1). It then prepares the
execution of the workflow run and persists the corresponding metadata in a
[MongoDB](https://www.mongodb.com/) database (2, 3). To support the processing
of many workflows in parallel, WESkit uses [Celery](https://docs.celeryq.dev/)
with [Redis](https://redis.io/) as a
message broker to communicate with the Celery workers (4, 5). For testing, the
Celery workers can execute the workflows directly in the worker containers.
However, in production deployments, the workflow engines are executed in a
high-throughput cluster (HTC). To this aim, the Celery workers contact the
cluster's head-node to submit the engines into the cluster (6), and then wait for
the execution result (7). Currently, WESkit supports
[Slurm](https://slurm.schedmd.com/) and [IBM
LSF](https://www.ibm.com/docs/en/slsfh) as cluster backends and interacts with
them via the SSH protocol. After the workflow engine concludes its execution,
the workflow run metadata are communicated back to the REST server via the results
broker Redis (8, 9). Workflow run metadata are then persisted into the MongoDB
database and are made available to the client via the REST API (10).


WESkit's design has a number of implications:


- The actual deployment can be adapted to the specific needs of the users (clients),
  because each component can be self-managed or make use of an externally
  managed service.
- The stateless REST server, the Celery workers, MongoDB, and Redis - which together represent the
  core architectural components of WESkit - can be independently replicated to
  modulate scalability and availability.
- The REST server and Celery workers require access to the same shared file
  system that is used in the execution backend to execute the workflows.
- Workflow engines are executed via their native command-line interfaces, 
  which makes it easy to include new workflow engines, and guarantees 
  a maximum level of compatibility with their regular usage.
  Furthermore, input/output data access and job management on the
  workload execution infrastructure is entirely managed by the workflow
  engines.

## The GA4GH Tool Registry Service API

The TRS API provides a means to access workflows and tools through a common,
uniform interface that specifies operations for retrieving workflow and
container descriptors, as well as associated metadata, either directly from the
service implementing the TRS API or by referencing records on external
registries. TRS resources are accessible via "TRS URIs"
that are constructed from the URL of the TRS
service and the resource-specific identifiers assigned by that service.

## Project goal

During the BioHackathon Europe 2021 [project 8, “Executing workflows
in the cloud with
WESkit”](https://github.com/elixir-europe/bioHackathon-projects-2021/tree/main/projects/8)
we worked on realizing the following demonstrator use case:


> A user authenticates in a suitable client application using ELIXIR AAI and
> receives a valid JSON Web Token (JWT).  With the JWT, the user sends a run
> request to the WESkit instance, triggering the execution of a specific
> workflow on the cloud platform. The workflow descriptor is specified by the
> `workflow_url` field by way of a TRS URI. The workflow is provided with a set
> of test inputs available at a WESkit cloud deployment. After receiving the
> run request, WESkit validates the JWT token, fetches the workflow via the TRS
> API, installs the workflow and then executes the workflow on the specified
> test data set. Once the workflow is finished, the user can download results
> from the cloud platform into their environment using the S3 protocol.


Specifically, we identified the following three distinct tasks as requirements
for WESkit to be able to meet this use case (Figure 1):


1. Have WESkit accept TRS URIs as valid payloads to the `workflow_url` API
   form field; then fetch the workflow through the TRS API, install, and
   finally run the workflow.
2. Deploy a WESkit instance at <https://weskit.bihealth.org>.
3. Expose output files, which are natively located on POSIX/NFS storage
   in the WESkit system, via the S3 protocol by using a POSIX->S3
   protocol bridge.


> **Note:** The client application used for authentication is not within the
> scope of this BioHackathon project. However, the user can use any active
> ELIXIR AAI JWT
> [https://login.elixir-czech.org/oidc/](https://login.elixir-czech.org/oidc/)
> for this demonstration.

# Project design

This section describes the assumptions and considerations that formed the basis
for the detailed project outline and the formulation of individual work
packages, drafted at the beginning of the hackathon.

## Components required to run a workflow

To execute a workflow, the following components are required.


- A workflow engine that interprets the workflow and coordinates the execution
  of the workflow jobs
- Workflow inputs
- The primary workflow descriptor is to be interpreted by the workflow engine,
  i.e., typically a `main.nf` file or a `Snakefile` for Nextflow and Snakemake,
  respectively (`PRIMARY_DESCRIPTOR`)
- Secondary workflow descriptors (e.g., subworkflows), if available
  (`SECONDARY_DESCRIPTOR`)
- Reference/test data, if available (`TEST_FILE`)
- Scripts, configuration files, software dependency files and any other files
  required for the workflow to be executed, if available (`OTHER`)


Except for workflow engines and input data, all other files can be
packaged in a TRS record and should ideally be available
as download from the TRS server. In the
list above, the string indicated in parentheses at the end, where available,
denotes the TRS file type of the corresponding file or class of files. Note
that for the `PRIMARY_DESCRIPTOR` file type, there must be exactly one entry in
a TRS workflow record. For all other types, there may be zero, one or multiple
files per TRS record.


For our demonstrator use case, we assumed that TRS records are indeed
_complete_ in the sense that they are sufficient to trigger a workflow run,
given a workflow engine and suitable inputs (or even without inputs if the TRS
record contains a complete set of test data).


## TRS service, test workflow and installation

For testing workflow access, we decided to use the
[V-pipe](https://github.com/cbg-ethz/V-pipe) Snakemake workflow for viral
pathogen analysis based on next-generation sequencing data.
The workflow is
co-authored by contributors to this hackathon project [@Posada_Cespedes_2020]
and is available on WorkflowHub [@Goble_2021] via the TRS API.


> In particular, at the time of the hackathon, the workflow was available on a
> development instance of WorkflowHub, via TRS identifier 230 and version
> identifier 1. At the time of writing (June 2022), that particular version was
> not available anymore.


As WESkit was designed to process sensitive data, the ability
to control which code is executed is of critical importance. Therefore, WESkit
was designed to operate with a single, system-wide, and administrator-managed
workflow installation directory. This directory resides on a file system that
is shared with the compute infrastructure executing the workflow (and which
also contains all workflow inputs and outputs). Therefore, we chose to install
the test workflow as fetched from the WorkflowHub TRS in that same directory.


Note that for the demonstrator use case, we executed the workflow directly in
the Celery workers, i.e., we did not make use of a workload management system
like [Slurm](https://slurm.schedmd.com/).

## TRS URIs

According to the [specification describing TRS URIs](https://github.com/ga4gh/tool-registry-service-schemas/blob/64e63d210f576acfac8a44290620e3987d942ac0/2.0.0-Data-Model.md#trs-uris)  at the time of the hackathon,
a TRS URI has the form

```console
trs://<server>/<id>
```  


According to the [WES API specification](https://github.com/ga4gh/workflow-execution-service-schemas/tree/c3b19854240c4fcbaf3483e22b19db0a918a7ee5) at the time of the hackathon,
the workflow to be executed must be provided by the `workflow_url` field of the
run request, either as a URL pointing to the workflow descriptor file or as a
reference to a file uploaded as a `workflow_attachment` to the run request. As
there is no restriction provided on the supported URI schemes, we concluded
that passing a TRS URI via the `workflow_url` field is valid.


Consequently, a WES run request with a TRS URI should then look similar to
the following minimal example:

  
```console
workflow_params" = "{}"
workflow_type" = "SMK"
workflow_type_version = "6.10.0"
tags = "{}"
workflow_engine_parameters = "{}"
workflow_url = "trs://dev.workflowhub.eu/230"
workflow_attachments = "[]"
```


These values are sent to the WESkit server as `multipart/form-data` payload via
the `POST /runs` request.


As stipulated by the TRS documentation, TRS URI need to be translated by client
applications such as WESkit into a URL that can be used in TRS requests, e.g.,


```console
https://<server>/ga4gh/trs/v2/tools/<id>
```
  

Based on this, the appropriate TRS endpoint to fetch the workflow descriptors
and other required files can in principle be constructed.

## TRS client

Rather than implementing a dedicated WESkit client to interact with the
WorkflowHub's TRS API, we decided to make use of the
[TRS-cli](https://github.com/elixir-cloud-aai/TRS-cli) library, a light-weight
TRS client library implemented in Python 3 (like WESkit itself) developed by
one of the hackathon participants.

# Project implementation

This section describes the actual implementation work conducted during the
hackathon and details the project outcomes. It also enumerates the pitfalls and
limitations encountered during the implementation phase and the approaches
taken to overcome them.

## Using TRS to retrieve V-pipe from WorkflowHub

Here, we describe the detailed approach taken to retrieve the V-pipe workflow
through WorkflowHub's TRS API.

### Extending the TRS URI definition

In initial tests, we decided to test workflow retrieval the using cURL.
To do that, we first needed to construct the
endpoint URIs corresponding to the TRS URI for V-pipe on WorkflowHub:


```console
trs://dev.workflowhub.eu/230
```


However, we quickly realized that the information in a TRS URI of this form is
insufficient to unambiguously identify a definite resource. This is because the
TRS API allows for multiple versions of a workflow to be stored under a given
tool (workflow) identifier.


Obviously, different versions of a given tool may lead to different results, and
therefore, the ability to clearly specify a tool version via a TRS URI is
critically important.


It is worth mentioning that the [definition of TRS URIs was added in response
to the definition of a similar
URI](https://github.com/ga4gh/tool-registry-service-schemas/commit/38d5bd8f2a76647d7693b98cab753371678c8cba)
in the GA4GH [Data Repository Service
(DRS)](https://github.com/ga4gh/data-repository-service-schemas) API [added
previously](https://github.com/ga4gh/data-repository-service-schemas/commit/9836246069c81d27dfca8e683da852a8af4a0051).
But unlike TRS, DRS does not specify the concept of versions for its resources.
We therefore concluded that the absence of a tool version in TRS URIs was
likely an oversight.


Therefore, during the hackathon, we filed issues in the GA4GH
[TRS](https://github.com/ga4gh/tool-registry-service-schemas/issues/164) and
[WES](https://github.com/ga4gh/workflow-execution-service-schemas/issues/175)
schema repositories with the goals to (1) extend the TRS URI definition by the
tool version, and (2) support TRS URIs, including the extension with tool
version identifiers, in the WES API, respectively.


```console
trs://<server>/<id>/<version>
```


> Note that a [pull
> request](https://github.com/ga4gh/tool-registry-service-schemas/pull/202) on
> the TRS issue provided by one of the hackathon participants has since
> been merged.


Furthermore, to simplify the implementation task for the week of the hackathon,
we decided to require the inclusion of more metadata for TRS URIs that WESkit
would be able to interpret:


1. First, to retrieve the workflow from WorkflowHub, WESkit needs to know the
   TRS [tool descriptor
   type](https://ga4gh.github.io/tool-registry-service-schemas/preview/containerfile_type_info/docs/index.html#operation/toolsIdVersionsVersionIdTypeDescriptorGet).
   Acceptable values were, for instance, `SMK`, `SMK_PLAIN`, `NFL`, and
   `NFL_PLAIN`. These tool descriptor types conflate the concepts of the
   workflow language (`SMK` and `NFL` for Snakemake and Nextflow, respectively)
   and a technical detail on how the workflow is to be downloaded from the TRS
   server (a plain text versus a JSON response). The WES API specification
   requires a field `workflow_type` when requesting a workflow run, which takes
   values such as `SMK` and `NFL`. Thus, the TRS tool descriptor type could in
   principle be derived from that field in the WES API call. However, unlike
   the tool descriptor type, the `workflow_type` field only represent the
   workflow language. While the decision whether to augment the `workflow_type`
   value with `_PLAIN` or not could be argued to be a WES/WESkit implementation
   detail, the purpose of the demonstrator use case, we kept it simple and
   included the tool descriptor type in the TRS URIs. However, we harmonized
   the `workflow_type` values accepted by WESkit with the TRS tool descriptor
   types, by changing them from `Snakemake` and `Nextflow` to `SMK` and `NFL`,
   respectively.
2. Secondly, WESkit also needs to know the relative path to the primary
   workflow descriptor. As was mentioned previously, this information is
   available in TRS as a file type annotation (`PRIMARY_DESCRIPTOR`). However,
   for improved clarity and simplicity, we have decided to include the relative
   path to the primary workflow descriptor in the TRS URI as well.


As a result, a WESkit-compliant TRS URI has the form:


```console
trs://<server>/<id>/<version>/<type>/<workflow_path>
```

For instance, we used the following WESkit-compliant TRS URI to access the
V-pipe workflow on WorkflowHub:


```console
trs://dev.workflowhub.eu/230/1/SMK/Snakefile`
```


## Extending TRS-cli

After we were able to access V-pipe on WorkflowHub via cURL requests, we tried
to do the same with the TRS-cli library. To achieve that, several changes were
implemented for TRS-cli, as briefly summarized here:


- Support for the `SMK` descriptor type was added
  ([commit](https://github.com/elixir-cloud-aai/TRS-cli/commit/64c4832ccfec2b215bfacb5362a86437c6613533))
- An option was added to pass TRS URIs that are not already URL-encoded
  ([commit](https://github.com/elixir-cloud-aai/TRS-cli/commit/b1ef580c17007ae55db7d98da8776c5df512589a))
- Support for returning a ZIP archive of all files included in a TRS record was
  added, as required by the TRS specification
  ([commit](https://github.com/elixir-cloud-aai/TRS-cli/commit/e8ed145f685eb8844525ac392fdd82964617bde8))
- Support for the WESkit-compliant TRS URI format as described above was added
  ([commit](https://github.com/elixir-cloud-aai/TRS-cli/commit/2fbb0d8e8cbe2597dc9029ef7fb5401f65dadc39))


In addition to these changes that were required to implement retrieval of
workflows via TRS in WESkit, a total of [nine additional code
changes](https://github.com/elixir-cloud-aai/TRS-cli/commits/dev) were merged
into the TRS-cli code base during the hackathon to address bugs or improve the
user experience.


## Installing TRS workflows in WESkit

As mentioned previously, we wanted to make use of WESkit's centrally managed
workflow directory on a file system shared with the execution environment, so
we already knew which root directory to choose to store workflows fetched from
TRS in. However, we had not yet decided how to organize its
subdirectories. Given that these would appear in WESkit's workflow run
logs, we decided against opaque UUID-based subdirectory names. Instead, we
decided to use a directory structure that includes the basic workflow metadata
from the extended TRS URI we defined previously. For instance, the TRS URI


```console
trs://dev.workflowhub.eu/230/1/SMK
```


is mapped to the directory


```bash
$WESKIT_WORKFLOWS_DIRECTORY/230/1/SMK_PLAIN/SMK
```


Here, `WESKIT_WORKFLOWS` refers to the root workflow directory that WESkit
mounts into the service containers. For the significance of `SMK_PLAIN`, see
the description of the TRS URI extension above. Finally, the terminal `SMK`
refers to the WESkit-internal name of the used workflow engine (here:
`snakemake`). In the future, we are planning on omitting the `SMK_PLAIN` part,
as it does not add any additional information that is of significance to the
user. This directory layout allows the user to immediately recognize the type,
as well as the TRS tool and tool version identifiers of the executed workflow.


After deciding on the directory structure, we imported TRS-cli to fetch a ZIP
archive composed of all files associated with the V-pipe TRS record. These
included the primary workflow descriptor, secondary workflow descriptors, test
data, reference data, and Conda environment specifications for the workflow
jobs.


The generic endpoint URL for accessing the ZIP archive from TRS is:


```console
https://<server>/tools/<id>/versions/<version>/<type>/files?format=zip
```


Consequently, to retrieve a ZIP archive of V-pipe, WESkit used the following
URL:


```console
https://dev.workflowhub.eu:443/tools/230/versions/1/SMK_PLAIN/files?format=zip
```


### Concurrency

The WESkit REST server is implemented in a stateless fashion based on the
[Flask](https://palletsprojects.com/p/flask/) framework. The general aim is to
provide scalability by allowing the processing of multiple run requests
concurrently. This design goal also requires that run requests for new
workflows, i.e., workflows that first need to be downloaded from a TRS server,
should be handled in parallel.


Workflow retrieval and installation was implemented in such a way that it is
handled by the REST server during the processing of the corresponding WES API
workflow run request. This has the advantage that the workflow is properly set
up by the time the REST server returns the workflow run identifier as a
response to the user. However, the workflow run request blocks the REST server
for the duration of handling the request, and so this design increased the
duration of the block for the amount of time it takes to retrieve and install
the workflow files. We assume that the size of these files is generally small,
so the application will only be blocked for a few seconds more at most.
However, if the TRS service is unavailable, blocking of the application
will be at least as long as the configured timeout.


To ensure that an incoming workflow run request will not accidentally override
an ongoing download or installation of that very same workflow, we have decided
to create a lock file in the base workflow directory using the
[`flufl.lock`](https://gitlab.com/warsaw/flufl.lock) package. `flufl.lock`
provides an NFS-safe, file-based locking mechanism that fits well with the
shared file system architecture of WESkit.


While a workflow installs and the lock for the installation directory is
maintained, none of the other run requests that try to access that same
workflow can proceed. Consequently, their run request API calls are blocked
for as long as the installation takes. With an unavailable TRS
server or with a high submission rate of run requests, the WESkit REST server
may quickly run out of threads to serve requests, even those that do not
require the installation of workflows. For production usage, we may therefore
need to redesign this approach or ensure that a sufficient number of REST
servers are available.


> Note that we only tested the directory-based locking between multiple uWSGI
> server threads, which are implemented as UNIX processes. We did not test the
> locking mechanism with multiple instances of the REST container, neither on a
> single host nor on multiple hosts.


## Installing workflow job dependencies

As mentioned previously, we had decided that for the scope of the demonstrator
use case we would run the test workflow directly in the Celery worker. This
required updating Snakemake in the worker's Conda environment to a recent
version (`v6.10.0`).


As our test workflow relies on Conda environments to manage the dependencies
of each workflow job, we then tested the concurrent installation of such
environments on the worker nodes. It turned out that the installation of new
environments by Conda is not concurrency-safe.


As we did not implement any measures to avoid data corruption during the
installation of Conda environments, the consequence is that, currently, the
execution of workflows requiring Conda environments is not concurrency-safe if
there is no strategy in place to guarantee the encapsulation of jobs from one
another.


Note that in principle it is possible to configure the workflow engine to make
run-specific installations of the Conda environments. But this will incur high
costs from increased storage and network resource consumption. On the other
hand, for workflows dealing with sensitive data, single-tenancy may be
preferable for security reasons.


This issue will require more research and a careful weighing of options.


# Cloud deployment

In order to roll out the cloud environment for the demonstrator use case,
platform, multiple steps and extensions of WESkit were required.


## WESkit deployment

First, we deployed the required WESkit environment at the Charité site of the
de.NBI Cloud, according to the deployment described in Figure 1. The
environment is available at <https://weskit.bihealth.org>.


The respective deployment scripts were integrated into the deployment
repository available on the WESkit project space on GitLab. To deploy future
versions of WESkit also directly to the de.NBI Cloud virtual machines, we
integrated a deployment script into the WESkit CI/CD pipeline on GitLab. A
difficulty here was the firewall policy at the de.NBI Cloud, which blocked
access from Gitlab servers in the US to the de.NBI cloud virtual machines.
This restriction was paused temporarily during the hackathon, but there is
still a requirement to find a stable solution for automated deployments from
<https://gitlab.com> to a node in the de.NBI Cloud.


Finally, the deployed WESkit instance was registered as a relying service with
ELIXIR AAI, which was since replaced with the "[Life Science
Login](https://lifescience-ri.eu/ls-login.html)".


## S3 support in WESkit

To allow users to download output files directly from the cloud platform, we
decided to support S3 on the cloud platform and within WESkit. In particular,
we provided S3 support to the cloud platform by deploying an instance of
[MinIO](https://min.io/) in such a way that it could access the same file
system as the WESkit instance (Figure 1). The WESkit data folder was mounted as
a data folder in MinIO. With this configuration, each new workflow run
directory created by WESkit is interpreted as new S3 bucket by MinIO. To access
the MinIO server, we also registered two additional subdomains for the WESkit
cloud deployment, the first for accessing the MinIO console
[weskit.s3-console.bihealth.org](<https://weskit.s3.bihealth.org>), the second
for accessing the corresponding S3 endpoint
[weskit.s3.bihealth.org](<https://weskit.s3.bihealth.org>).


We have also extended WESkit's deployment repository with the instructions to
co-deploy a MinIO service so that this functionality can be easily tested on
local user deployments.


Next, WESkit was extended with the functionality to return pre-signed URLs for
`GET /runs/{run_id}` requests. The WESkit extension uses the
`generate_presigned_url()` function from the `boto3` package, which can create a
pre-signed URL for a specified S3 object. As mentioned above, each WESkit
workflow run directory is treated by MinIO as its own S3 bucket, and therefore
all files in those directories are accessible as S3 objects via MinIO.
Specifically, the pre-signed URLs for each object are available in the
`RunLog.outputs` field of WESkit's `GET /runs/{run_id}` WES API endpoint,
together with the relative paths to the files. Using these pre-signed URLs,
users can conveniently fetch these files from the MinIO server using a suitable
client.


# Conclusion

WESkit's first BioHackathon turned out to be a very productive event that
offered ample opportunity for cooperation and networking. The coherence of the
groups in Berlin, Heidelberg, and Basel, who were already involved in WESkit,
was strengthened, and additional interactions with the groups working on
WorkflowHub and Sapporo helped us to be productive and improve our system.
Furthermore, the support of BioHackathon participants previously unrelated to
the project was essential for the success of both the aims concerning TRS
and S3 support.


We implemented basic TRS support. WESkit can interpret URIs of the form
`trs://<server>/<id>/<version>/<type>/<workflow_path>` that it receives via the
`workflow_url` field of run requests. Compared to the original TRS URI format
proposed by GA4GH, these URIs contain additional information. Our opinion that
the definition of TRS URIs should at least be extended by the workflow version
was shared by the community, as our proposal in this regard was since merged
into the official documentation accompanying the TRS API specification.


The TRS (tool descriptor) type can be inferred from the `workflow_type` field
of the WES API run request. This inference would be trivial if the same set of
workflow type values were guaranteed to be used across the WES and TRS APIs.
Note that a coordinated vocabulary for workflow types is indeed being
[discussed](https://github.com/ga4gh/workflow-execution-service-schemas/issues/173).
Finally, we also require that the
relative path to the workflow file, e.g. `workflow/Snakefile`, is specified in
the URI. In the future, we plan to obtain this information via the
`PRIMARY_DESCRIPTOR` file type in TRS, which - as per the specification - is
required to annotate exactly one file in a TRS workflow record.


The installation of the workflow descriptor and associated files from the TRS
server includes the download and installation of its files to the file system.
We decided to install the TRS-workflows in the central, shared workflow
installation directory and implemented a locking mechanism to prevent data
corruption. Nevertheless, with the current implementation, the retrieval and
installation of even a small workflow from TRS may completely block the WESkit
REST server if the client submits a large number of concurrent requests to
access this workflow, or if the TRS server is unavailable. However, similar
Denial of Service scenarios may arise when accessing workflows from other
external sources (e.g., GitHub) or when clients attach workflow files via the
`workflow_attachment` field. Increasing the number of API servers, placing an
HTTP server and load balancer in front of the REST API and request throttling
are some approaches that can be taken to improve WESkit scalability.


The realization that Conda installations are not concurrency-safe remains an
unresolved problem for the current implementation of WESkit when dealing with
workflows that rely on Conda environments for managing job dependencies.
Compared with fetching the files associated with a TRS record, the
installation of such environments may take orders of magnitude longer. While
Conda installations are done asynchronously on the Celery workers, the
current solution is not concurrency-safe. Consequently, the current
implementation will only work reliably if it can be guaranteed that jobs
executed in parallel will not concurrently install the same dependencies in the
same place.


While Conda-based workflows are widely used, state of the art in terms of 
achieving reproducibility are fully "containerized" workflows. 
Containers also have the advantage that the responsibility for
concurrency-safe software installation can then be delegated to a container
registry. For instance, we found that, e.g., pulling containers into the Docker
daemon is already concurrency-safe. Docker also supports a registry mode of
execution. We also made a quick evaluation of the
[Zot](https://github.com/project-zot/zot) registry and found it to be
concurrency-safe as well. In contrast to Docker, Zot is vendor-agnostic and can
handle various OCI-compliant container formats. Such a registry could function
as a central caching registry for a WESkit deployment, on-premise or in the
cloud.


The physical proximity of actively working groups and general integration of
the ELIXIR BioHackathon event turned out to be the crucial factor for the success
of the project. In at least two situations it was extremely helpful to have
short communication paths:


- One of the participants of our project is the main developer of the
  [TRS-cli](https://github.com/elixir-cloud-aai/TRS-cli) library that we used
  for accessing the TRS server in WESkit. This led to a total of 13 [commits
  in the corresponding
  repository](https://github.com/elixir-cloud-aai/TRS-cli/commits/dev)
  (November 9-11).
- Similarly, two minor compliance issues with the WorkflowHub TRS
  implementation were uncovered during the hackathon. As the
  responsible developer, Finn Bacall, was participating remotely in the event, we
  were able to reach out to him easily and the issues were resolved within
  minutes.


# Acknowledgements

We would like to express our gratitude to ELIXIR for sponsoring the ELIXIR
BioHackathon event and providing travel and lodging costs for two hackathon
participants. In particular, we would like to thank the organizing team for
their excellent work in making this event a memorable experience for all the
project participants. Finally, we would like to thank all other
participants, whether they attended in person or remotely, for the interesting
discussions, inspiration, and the nice evenings together. Among these, we are
particularly grateful for the interactions with the Sapporo team (project #22),
led by Hirotaka Suetake and Tazro Ohta, who are also working on GA4GH Cloud
APIs, as well as the effective help of Finn Bacall from the WorkflowHub team
(project #11).


# References
